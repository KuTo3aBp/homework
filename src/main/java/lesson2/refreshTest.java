package lesson2;

import static lesson2.Main.driver;

class refreshTest {

    /*
    * 1. Get current title
    * 2. Refresh page
    * 3. If title not changed - refresh test passed!
    * */
    static void run() {
        String titleBeforeRefresh = driver.getTitle();
        System.out.print("Current title: " + titleBeforeRefresh);
        driver.navigate().refresh();
        if (driver.getTitle().equals(titleBeforeRefresh)) {
            System.out.println(" refresh test passed!");
        }
        else
        {
            System.out.println(" something went wrong, please check!");
        }
    }
}
