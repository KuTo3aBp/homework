package lesson2;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static lesson2.login.doLogon;


class Main {

    static ChromeDriver driver;

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.gecko.driver", "C:\\path\\geckodriver.exe");
        System.setProperty("webdriver.chrome.driver", "C:\\path\\chromedriver.exe");

        final String URL = "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";
        final String LOGIN = "webinar.test@gmail.com";
        final String PASSWORD = "Xcg7299bnSmMuRLp9ITw";

        driver = new ChromeDriver();

        WebDriverWait wait = new WebDriverWait(driver, 30);  // set timeout time in seconds
        doLogon(URL, LOGIN, PASSWORD); // pass url, login, password

        // if we can see left menu - needed elements are loaded, and we can continue
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".maintab.has_submenu")));

        // Note: whole menu parsing can't be used because of DOM element recreation issue
        // (WebElements must be re-initialized every create/destroy cycle)
        // IMHO css array is most optimal and fast way to automate menu testing
        String cssString[] = {
                "#subtab-AdminParentOrders", "#subtab-AdminCatalog", "#subtab-AdminParentCustomer",
                "#subtab-AdminParentCustomerThreads", "#subtab-AdminStats", "#subtab-AdminParentModulesSf",
                "#subtab-AdminParentThemes", "#subtab-AdminParentShipping", "#subtab-AdminParentPayment",
                "#subtab-AdminInternational", "#subtab-ShopParameters", "#subtab-AdminAdvancedParameters"
        };

        System.out.println("Current title: " + driver.getTitle());
        String currentUrl = driver.getCurrentUrl(); // get current url for returning point

        // if we can see menu
        // click at submenu
        // run refresh test
        // go back to start
        for (String css : cssString) {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".maintab.has_submenu")));
            driver.findElement(By.cssSelector(css)).click();
            refreshTest.run();
            driver.navigate().to(currentUrl);
        }
    }
}